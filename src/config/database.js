module.exports = {
    dialect: 'mysql',
    host: 'localhost',
    username: 'user',
    password: 'pass',
    database: 'DB',
    define: {
        timestamps: true,
        underscored: true,
        underscoredAll: true,
    },
};
