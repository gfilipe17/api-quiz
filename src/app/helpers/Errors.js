const Errors = {
    QUIZ: {
        code: 'QUIZ_NOT_FOUND',
        status: 404,
        message: 'Quiz not found.',
    },
};

export default Errors;
